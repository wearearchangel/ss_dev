<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    /**
     * List the fields that can be mass assigned.
     * @var array
     */
    protected $fillable = ['name', 'path', 'type'];

    /**
     * Links to the user that created the file.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * Links to the group files that belong to the file.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function groupFile()
    {
        return $this->hasMany('App\GroupFile');
    }

    /**
     * Links to the personal files that belong to the file.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function personalFile()
    {
        return $this->hasMany('App\PersonalFile');
    }

    /**
     * Returns the path to the file.
     *
     * @return mixed
     */
    public function path()
    {
        return $this->path;
    }

    /**
     * Returns the type(extension) of the file.
     *
     * @return mixed
     */
    public function type()
    {
        return $this->type;
    }
}
