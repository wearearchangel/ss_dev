<?php

namespace app\Repos\File;


use App\GroupFolder;
use App\Traits\File\FileRepository;
use App\Traits\File\FileSharing;
use App\Traits\File\FileStorage;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class ManagerRepository
{
    use FileSharing;
    use FileRepository;

    /**
     * Store group files for the users.
     *
     * @param UploadedFile $file
     * @param GroupFolder|PersonalFolder $folder
     * @param $user
     * @param null $requestName
     * @return bool
     */
    public function storeGroupFile(UploadedFile $file,  GroupFolder $folder,User $user, $requestName=null)
    {

        $file  = $this->storeFile($file, $user, $requestName);

        $file->groupFile()->create([
            'name' => $file->name,
            'group_folder_id' => $folder->id,
        ]);

        return true;
    }

} 