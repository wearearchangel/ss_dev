<?php

namespace App\Repos\File;


use App\Interfaces\File\FolderModelInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class FileRepository
{
    protected $destination = 'uploads/';

    public function store(UploadedFile $file,  FolderModelInterface $folder, $user, $requestName=null)
    {
        $type = $file->getClientOriginalExtension();

        //Get the name for the file

        if($requestName == null)
            $name = $file->getClientOriginalName();
        else
        {
            $name = $this->sanitizeName($requestName).'.'.$type;
        }
        $actualName = time(). $name;

        // Set the destination for the uploaded files.

        $path = $this->destination . $actualName;

        // Move the file to the destination.

        if (!$file->move($path)) {
            return false;
        }

        //Persist the files to the database.

        $details = [
            'name' => $name,
            'path' => $path,
            'type' => $type,
        ];

        $file = $this->persist($details, $user);

        return true;
    }

    protected function sanitizeName($requestName)
    {
        $fileName = preg_replace("([^\w\s\d\-_~,;:\[\]\(\].]|[\.]{2,})", '', $requestName);
        $fileName = filter_var($fileName, FILTER_SANITIZE_URL);
        return $fileName;
    }

    protected function persist(Array $details, $user)
    {
         return $user->files()->create([
            'name' => $details['name'],
            'path' => $details['path'],
            'type' => $details['type'],
         ]);
    }
} 