<?php


namespace App\Repos\File;


use App\User;
use App\PersonalFolder;
use App\Traits\File\FileSharing;
use App\Traits\File\FileRepository;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class PackRepository
{
    use FileSharing;
    use FileRepository;

    /**
     * Store personal files for the users.
     *
     * @param UploadedFile $file
     * @param PersonalFolder $folder
     * @param $user
     * @param null $requestName
     * @return bool
     */
    public function storePersonalFile(UploadedFile $file,  PersonalFolder $folder, User $user, $requestName=null)
    {
        $file  = $this->storeFile($file, $user, $requestName);

        $file->personalFile()->create([
            'name' => $file->name,
            'personal_folder_id' => $folder->id,
        ]);

        return true;
    }
} 