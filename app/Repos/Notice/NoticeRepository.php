<?php

namespace App\Repos\Notice;

use App\Notice;

use App\Traits\Post\Postable;

class NoticeRepository
{
    use Postable;

    /**
     * Creates a new notice.
     *
     * @param $request
     * @param $group
     */

    public function createNotice ($request, $group)
    {
        $notice = $group->notices()->create(
            [
                'title' => $request->title,
                'message' => $request->message,
                'user_id' => \Auth::user()->id,
            ]
        );

        return $notice;
    }


    /**
     * Updates authenticated user notices.
     *
     * @param $noticeId
     * @param $request
     */
    public function updateUserNotice($noticeId, $request)
    {
        //get the pin to be updated
        $notice = Notice::where('id', '=', $noticeId)->first();

        //update the pin title and message
        $notice->fill([
                'title' => $request->title,
                'message' => $request->message,
            ]
        )->save();
    }

    /**
     * Retrieve a single notice for the authenticated user
     *
     * @param $noticeId
     * @return mixed
     */
    public function getUserNotice($noticeId)
    {
        //return the detail of the pin to allow the detail to be updated
        return Notice::where('id', '=', $noticeId)->first();
    }



}