<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

get('/', function () {
    return view('ss.home.index');
});


// Authentication routes...
get('auth/login', 'Auth\AuthController@getLogin');
post('auth/login', 'Auth\AuthController@postLogin');
get('auth/logout', 'Auth\AuthController@getLogout');


// Registration routes...
get('auth/register', 'Auth\AuthController@getRegister');
post('auth/register', 'Auth\AuthController@postRegister');

//Redirect routes after user registration

get('dashboard/lecturer', 'Dashboard\DashboardController@getLecturerDashboard');

get('dashboard/student', 'Dashboard\DashboardController@getStudentDashboard');
get('join-group/student', 'Dashboard\DashboardController@joinInitialGroup');

get('finish-join-group', 'Dashboard\DashboardController@finishJoinGroup');