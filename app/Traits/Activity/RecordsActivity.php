<?php

namespace App\Traits\Activity;


use App\Activity;

trait RecordsActivity {

    protected static function boot()
    {
        parent::boot();

        foreach (static::getModelEvents() as $event)
        {
            static::$event(function($model, $event)
            {
                $model->addActivity($event);
            });
        }

    }

    protected function getActivityName($model, $action)
    {
        $name = strtolower(new \ReflectionClass($model))->getShortName();
        return "{$action}_{$name}";
    }

    protected function addActivity($event)
    {
        Activity::create([
            'type' => $this->getActivityName($this, $event)
        ]);
    }

    protected function getModelEvents()
    {
        if(isset(static::$recordEvents))
        {
            return static::$recordEvents;
        }

        return ['created', 'updated', 'deleted'];
    }
} 