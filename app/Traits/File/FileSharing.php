<?php

namespace App\Traits\File;

use App\GroupFolder;
use App\Interfaces\File\FileModelInterface;

trait FileSharing {

    /**
     * @param FileModelInterface $file
     * @param GroupFolder $folder
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function shareFile(FileModelInterface $file, GroupFolder $folder)
    {
        return $folder->files()->create([
            'name' => $file->name,
            'file_id' => $file->file()->first()->id
        ]);
    }
} 