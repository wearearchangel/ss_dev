<html>
<head>
    <meta charset="UTF-8">
    <title>SS +</title>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="{{ asset('/css/bootstrap.min.css') }}">


</head>
<body>


@include('ss.nav.top.index')

<div class="container">
    @yield('content')
</div>


<!-- compiled and minified javascript -->
<script src="{{ asset('/js/jquery.min.js') }}"></script>

<!-- Latest compiled and minified JavaScript -->
<script src="{{ asset('/js/bootstrap.min.js') }}"></script>

</body>
</html>